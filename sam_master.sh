#!/bin/bash
#Controls the running of the python programs
#All echo command are purely for testing purposes, may be commented out 

#TODO

hour=$(date +"%H")

#If $(ps aux | grep -i program.py | wc -l) returns greater than 1, then program.py is running
check=1

#Run kill at midnight to clean the following
if [ "$hour" -eq 0 ]
then
    pkill -9 -f follows.py
    pkill -9 -f unfollows.py
    echo "hour=0"
    var=$(ps aux | grep -i genocide.py | wc -l)
    if [ "$var" != "$check" ]
    then
        echo "Kill Running"
    else
        echo "Kill Not Running, Trying To Restart"
        python3 /home/ubuntu/instatism/sam-bot/genocide.py
    fi
fi

#Run spicy in the morning to generate growth (1:00 - 7:59)
if [ "$hour" -ge 1 ] && [ "$hour" -le 23 ]
then
    pkill -9 -f genocide.py
    echo "hour=1-7"
    var=$(ps aux | grep -i follows.py | wc -l)
    var2=$(ps aux | grep -i unfollows.py | wc -l)
    if [ "$var" != "$check" ]
    then
        echo "Follow Running"
    else
        echo "Follow Not Running, Trying To Restart"
        python3 /home/ubuntu/instatism/sam-bot/follows.py
    fi
    if [ "$var2" != "$check" ]
    then
        echo "'unFollow Running"
    else
        echo "unFollow Not Running, Trying To Restart"
        python3 /home/ubuntu/instatism/sam-bot/unfollows.py
    fi
fi


#Always run mild for consant activity
var=$(ps aux | grep -i comment.py | wc -l)
var2=$(ps aux | grep -i likes.py | wc -l)
if [ "$var" != "$check" ]
then
    echo "Running Comment/Likes"
else
    echo "Mild Not Running, Trying To Restart"
    python3 /home/ubuntu/instatism/sam-bot/comment.py
fi
if [ "$var2" != "$check" ]
then
    echo "Running Comment/Likes"
else
    echo "Mild Not Running, Trying To Restart"
    python3 /home/ubuntu/instatism/sam-bot/likes.py
fi

