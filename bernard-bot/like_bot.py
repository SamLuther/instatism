#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time

from src import InstaBot
from src.check_status import check_status
from src.feed_scanner import feed_scanner
from src.follow_protocol import follow_protocol
from src.unfollow_protocol import unfollow_protocol

bot = InstaBot(
    login="ceres.patrik",
    password="pASSword312",
    like_per_day=1440,
    comments_per_day=0,
    tag_list=['follow4follow','f4f','instagood','supertags','followforfollow',
              'follow','l4l','outdoor', 'photography', 'linux', 'mountains',
              'softwareengineer','softwareengineering'],
    tag_blacklist=['nsfw', 'sex', 'fuck','cunt','bitch','politics','political',
                   'ass','twat','titties','tiddies','boobs','boobies','tits',
                   'political','cosmetics','makeup','lipstick','cuck','women',
                   'girls','sexy','porn','almostporn','pornhub','brazzers','messedup',
                   'balls','clinton','trump','sanders','anime','curvy','nips','nipples'],
    user_blacklist={},
    max_like_for_one_tag=50,
    follow_per_day=0,
    follow_time=1 * 60,
    unfollow_per_day=0,
    unfollow_break_min=15,
    unfollow_break_max=30,
    log_mod=0,
    proxy='',
    comment_list=[['A'],
                  ['generic'],
                  ['example'],
                  ['comment'],
                  ['.']],
    unwanted_username_list=[],
    unfollow_whitelist=['samluther998', 'bernardkintzing', 'usinterior'])
while True:

    #print("# MODE 0 = ORIGINAL MODE BY LEVPASHA")
    #print("## MODE 1 = MODIFIED MODE BY KEMONG")
    #print("### MODE 2 = ORIGINAL MODE + UNFOLLOW WHO DON'T FOLLOW BACK")
    #print("#### MODE 3 = MODIFIED MODE : UNFOLLOW USERS WHO DON'T FOLLOW YOU BASED ON RECENT FEED")
    #print("##### MODE 4 = MODIFIED MODE : FOLLOW USERS BASED ON RECENT FEED ONLY")
    #print("###### MODE 5 = MODIFIED MODE : JUST UNFOLLOW EVERYBODY, EITHER YOUR FOLLOWER OR NOT")

    ################################
    ##  WARNING   ###
    ################################

    # DON'T USE MODE 5 FOR A LONG PERIOD. YOU RISK YOUR ACCOUNT FROM GETTING BANNED
    ## USE MODE 5 IN BURST MODE, USE IT TO UNFOLLOW PEOPLE AS MANY AS YOU WANT IN SHORT TIME PERIOD

    mode = 0

    if mode == 0:
        bot.new_auto_mod()

    elif mode == 1:
        check_status(bot)
        while bot.self_following - bot.self_follower > 200:
            unfollow_protocol(bot)
            time.sleep(10 * 60)
            check_status(bot)
        while bot.self_following - bot.self_follower < 400:
            while len(bot.user_info_list) < 50:
                feed_scanner(bot)
                time.sleep(5 * 60)
                follow_protocol(bot)
                time.sleep(10 * 60)
                check_status(bot)

    elif mode == 2:
        bot.bot_mode = 1
        bot.new_auto_mod()

    elif mode == 3:
        unfollow_protocol(bot)
        time.sleep(10 * 60)

    elif mode == 4:
        feed_scanner(bot)
        time.sleep(60)
        follow_protocol(bot)
        time.sleep(10 * 60)

    elif mode == 5:
        bot.bot_mode = 2
        unfollow_protocol(bot)

    else:
        print("Wrong mode!")
