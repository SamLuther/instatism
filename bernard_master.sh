#!/bin/bash
#Controls the running of the python programs
#All echo command are purely for testing purposes, may be commented out 

hour=$(date +"%H")

#If $(ps aux | grep -i program.py | wc -l) returns greater than 1, then program.py is running
check=1

#adjust time to match mtn time
timeChange=12
hourChanged=hour-timeChange

#Run kill at midnight to clean the following
if [ "$hourChanged" -eq 0 ]
then
    pkill -9 -f main_bot.py
    echo "hour=0"
    var=$(ps aux | grep -i death_bot.py | wc -l)
    if [ "$var" != "$check" ]
    then
        echo "Death Bot Running"
    else
        echo "Death Bot Not Running, Trying To Restart"
        python3 /home/ubuntu/instatism/bernard-bot/death_bot.py
    fi
fi

#Reset the programs at noon
if [ "$hour" -ge 12 ]
then
    pkill -9 -f death_bot.py
    pkill -9 -f main_bot.py
    pkill -9 -f like_bot.py
fi

#Run the bot for most of the day (1:00 - 23:59)
if [ "$hour" -ge 1 ] && [ "$hour" -le 23 ]
then
    pkill -9 -f death_bot.py
    echo "hour=1-23"
    var=$(ps aux | grep -i main_bot.py | wc -l)
    if [ "$var" != "$check" ]
    then
        echo "Main Bot Running"
    else
        echo "Main Bot Not Running, Trying To Restart"
        python3 /home/ubuntu/instatism/bernard-bot/main_bot.py
    fi
fi

#Always run the liking bot for consant activity
var=$(ps aux | grep -i like_bot.py | wc -l)
if [ "$var" != "$check" ]
then
    echo "Like Bot Running"
else
    echo "Like Bot Not Running, Trying To Restart"
    python3 /home/ubuntu/instatism/bernard-bot/like_bot.py
fi
